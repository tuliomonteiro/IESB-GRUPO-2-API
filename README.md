# API para o projeto PPC do Grupo 2

### Alunos

* José Eduardo Kimura Reis - 17186101035

* Rodrigo Hoshino Okiyama - 17186101024

* João Vinicius Fernandes Oliveira - 17186101034

* Macilenio Holanda Oliveira - 17186101011

## Dependências

* Node >= 8
* yarn >= 1.7

## Instalação

```sh
$ yarn install

```

## Iniciando o servidor em ambiente local

```sh
$ yarn run start

```