'use strict';
module.exports = (sequelize, DataTypes) => {
  const Curso = sequelize.define('Curso', {
    tipoCurso: DataTypes.STRING,
    modalidade: DataTypes.STRING,
    denominacaoCurso: DataTypes.STRING,
    habilitacao: DataTypes.STRING,
    localOferta: DataTypes.STRING,
    turnoFuncionamento: DataTypes.STRING,
    numVagas: DataTypes.INTEGER,
    cargaHora: DataTypes.INTEGER,
    regimeLetivo: DataTypes.STRING,
    periodos: DataTypes.STRING,
    professorNome:DataTypes.STRING
  }, {});
  Curso.associate = function(models) {
    // associations can be defined here
  };
  return Curso;
};