'use strict';
module.exports = (sequelize, DataTypes) => {
  const Professor = sequelize.define('Professor', {
    fullName: DataTypes.STRING,
    cpf: DataTypes.STRING,
    highestDegree: DataTypes.STRING,
    major: DataTypes.STRING,
    curriculumLattes: DataTypes.STRING
  }, {});
  Professor.associate = function(models) {
    // associations can be defined here
  };
  return Professor;
};