'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bibliografia = sequelize.define('Bibliografia', {
    titulo: DataTypes.STRING,
    autor: DataTypes.STRING,
    isbn: DataTypes.STRING,
    ano: DataTypes.INTEGER,
    editora: DataTypes.STRING,
    nomeCurso: DataTypes.STRING,
    nomeDisciplina: DataTypes.STRING
  }, {});
  Bibliografia.associate = function(models) {
    // associations can be defined here
  };
  return Bibliografia;
};