'use strict';
module.exports = (sequelize, DataTypes) => {
  const AtaReuniao = sequelize.define('AtaReuniao', {
    data: DataTypes.DATE,
    local: DataTypes.STRING,
    listaParticipantes: DataTypes.ARRAY(DataTypes.STRING),
    deliberacoes: DataTypes.STRING
  }, {});
  AtaReuniao.associate = function(models) {
    // associations can be defined here
  };
  return AtaReuniao;
};