'use strict';
module.exports = (sequelize, DataTypes) => {
  const Disciplina = sequelize.define('Disciplina', {
    nome: DataTypes.STRING,
    descricao: DataTypes.STRING,
    codigo: DataTypes.STRING,
    semestre: DataTypes.STRING,
    cargaHoraria: DataTypes.STRING
  }, {});
  Disciplina.associate = function(models) {
    // associations can be defined here
  };
  return Disciplina;
};