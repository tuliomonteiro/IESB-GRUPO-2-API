'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProjetoPedagogico = sequelize.define('ProjetoPedagogico', {
    curso: DataTypes.STRING,
    perfilCursos: DataTypes.STRING,
    perfilEgresso: DataTypes.STRING,
    formaAcessoCurso: DataTypes.STRING,
    graficoPerfilFormacao: DataTypes.STRING,
    sistemaAvaliacaoAprendizagem: DataTypes.STRING,
    avaliacaoProjetoCurso: DataTypes.STRING,
    tcc: DataTypes.BOOLEAN,
    estagioCurricular: DataTypes.BOOLEAN,
    politicaAtendimentoPcd: DataTypes.STRING
  }, {});
  ProjetoPedagogico.associate = function(models) {
    // associations can be defined here
    
  };
  return ProjetoPedagogico;
};