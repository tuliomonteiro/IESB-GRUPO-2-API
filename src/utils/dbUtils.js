const Sequelize = require('sequelize');

const sequelize = new Sequelize('localhost', 'ppc-grupo2', 'senha123', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
}); 

module.exports.sequelize = sequelize;