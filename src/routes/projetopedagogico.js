const express = require('express');
const router = express.Router();
const model = require('../../models/index');

router.get('/', (req, res, next) => {
  model.ProjetoPedagogico.findAll({})
    .then(projetoPedagogico => res.json({
      data: projetoPedagogico
    }))
    .catch(error => res.status(404).json({error}));
});

router.get('/:id', (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  model.ProjetoPedagogico.findById(id)
    .then(projetoPedagogico => {
      if (!projetoPedagogico) {
        return res.status(404).json({ error: 'Not Found' })
      }
      return res.json({ data: projetoPedagogico })
    })
    .catch(error => res.status(404).json({ error }));
});

router.post('/', function(req, res, next) {
  model.ProjetoPedagogico.create(req.body).then(projetoPedagogico => {
    if (!projetoPedagogico) {
      return res.status(400).json({ error: 'Error creating projetoPedagogico.'});
    }

    return res.status(201).json({
      data: projetoPedagogico,
    });
  }).catch(error => res.json({error}));
});

router.put('/:id', (req, res, next) => {
  const id = parserInt(req.params, 10);

  model.ProjetoPedagogico.update(req.body, { where: { id } })
    .then(projetoPedagogico => {
      if (!projetoPedagogico) {
        return res.status(304).json({ error: 'Not modified.'});
      }
      return res.status(200).json({ data: projetoPedagogico })
    })
    .catch(error => res.status(304).json({}))
});

router.delete('/:id', (req, res, next) => {
  const { id } = parseInt(req.params.id, 10);
  model.ProjetoPedagogico.destroy({ where: {
    id,
  }}).then(() => res.status(203).json({}))
  .catch(error => res.json({ error }));
});

module.exports = router;