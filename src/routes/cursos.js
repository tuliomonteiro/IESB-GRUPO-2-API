const express = require('express');
const router = express.Router();
const model = require('../../models/index');

router.get('/', (req, res, next) => {
  model.Curso.findAll({})
    .then(cursos => res.json({
      data: cursos
    }))
    .catch(error => res.status(404).json({error}));
});

router.get('/:id', (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  model.Curso.findById(id)
    .then(curso => {
      if (!curso) {
        return res.status(404).json({ error: 'Not Found' })
      }
      return res.json({ data: curso })
    })
    .catch(error => res.status(404).json({ error }));
});

router.post('/', function(req, res, next) {
  model.Curso.create(req.body).then(curso => {
    if (!curso) {
      return res.status(400).json({ error: 'Error creating curso.'});
    }

    return res.status(201).json({
      data: curso,
    });
  }).catch(error => res.json({error}));
});

router.put('/:id', (req, res, next) => {
  const id = parserInt(req.params, 10);

  model.Curso.update(req.body, { where: { id } })
    .then(curso => {
      if (!curso) {
        return res.status(304).json({ error: 'Not modified.'});
      }
      return res.status(200).json({ data: curso })
    })
    .catch(error => res.status(304).json({}))
});

router.delete('/:id', (req, res, next) => {
  const { id } = parseInt(req.params.id, 10);
  model.Curso.destroy({ where: {
    id,
  }}).then(() => res.status(203).json({}))
  .catch(error => res.json({ error }));
});

module.exports = router;