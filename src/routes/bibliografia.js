const express = require('express');
const router = express.Router();
const model = require('../../models/index');

router.get('/', (req, res, next) => {
  model.Bibliografia.findAll({})
    .then(bibliografias => res.json({
      data: bibliografias
    }))
    .catch(error => res.status(404).json({error}));
});

router.get('/:id', (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  model.Bibliografia.findById(id)
    .then(bibliografia => {
      if (!bibliografia) {
        return res.status(404).json({ error: 'Not Found' })
      }
      return res.json({ data: bibliografia })
    })
    .catch(error => res.status(404).json({ error }));
});

router.post('/', function(req, res, next) {
  model.Bibliografia.create(req.body).then(bibliografia => {
    if (!bibliografia) {
      return res.status(400).json({ error: 'Error creating bibliografia.'});
    }

    return res.status(201).json({
      data: bibliografia,
    });
  }).catch(error => res.json({error}));
});

router.put('/:id', (req, res, next) => {
  const id = parserInt(req.params, 10);

  model.Bibliografia.update(req.body, { where: { id } })
    .then(bibliografia => {
      if (!bibliografia) {
        return res.status(304).json({ error: 'Not modified.'});
      }
      return res.status(200).json({ data: bibliografia })
    })
    .catch(error => res.status(304).json({}))
});

router.delete('/:id', (req, res, next) => {
  const { id } = parseInt(req.params.id, 10);
  model.Bibliografia.destroy({ where: {
    id,
  }}).then(() => res.status(203).json({}))
  .catch(error => res.json({ error }));
});

module.exports = router;