const express = require('express');
const router = express.Router();
const model = require('../../models/index');

router.get('/', (req, res, next) => {
  model.Professor.findAll({})
    .then(professors => res.json({
      data: professors
    }))
    .catch(error => res.status(404).json({error}));
});

router.get('/:id', (req, res, next) => {
  const id = parseInt(req.params.id, 10);
  model.Professor.findById(id)
    .then(professor => {
      if (!professor) {
        return res.status(404).json({ error: 'Not Found' })
      }
      return res.json({ data: professor })
    })
    .catch(error => res.status(404).json({ error }));
});

router.post('/', function(req, res, next) {
  model.Professor.create(req.body).then(professor => {
    if (!professor) {
      return res.status(400).json({ error: 'Error creating professor.'});
    }

    return res.status(201).json({
      data: professor,
    });
  }).catch(error => res.json({error}));
});

router.put('/:id', (req, res, next) => {
  const id = parserInt(req.params, 10);

  model.Professor.update(req.body, { where: { id } })
    .then(professor => {
      if (!professor) {
        return res.status(304).json({ error: 'Not modified.'});
      }
      return res.status(200).json({ data: professor })
    })
    .catch(error => res.status(304).json({}))
});

router.delete('/:id', (req, res, next) => {
  const { id } = parseInt(req.params.id, 10);
  model.Professor.destroy({ where: {
    id,
  }}).then(() => res.status(203).json({}))
  .catch(error => res.json({ error }));
});

module.exports = router;