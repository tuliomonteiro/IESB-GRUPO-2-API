'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ProjetoPedagogicos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      curso: {
        type: Sequelize.STRING
      },
      perfilCursos: {
        type: Sequelize.STRING
      },
      perfilEgresso: {
        type: Sequelize.STRING
      },
      formaAcessoCurso: {
        type: Sequelize.STRING
      },
      graficoPerfilFormacao: {
        type: Sequelize.STRING
      },
      sistemaAvaliacaoAprendizagem: {
        type: Sequelize.STRING
      },
      avaliacaoProjetoCurso: {
        type: Sequelize.STRING
      },
      tcc: {
        type: Sequelize.BOOLEAN
      },
      estagioCurricular: {
        type: Sequelize.BOOLEAN
      },
      politicaAtendimentoPcd: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ProjetoPedagogicos');
  }
};